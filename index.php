<?php

class ToDO
{

    /**
     * Instance de PDO
     *
     * @var PDO $_PDO
     */
    protected $_PDO;

    public function __construct()
    {
        $this->pdoConnection();
    }

    /**
     * Retourne les variables d'environnements
     *
     * @return array
     */
    protected function getEnv(): array
    {
        require_once 'env.php';

        if (!isset($BDD_HOST) || !isset($BDD_NAME) || !isset($BDD_USER) || !isset($BDD_PASS)) {
            die("Variables d'environnement non définies.");
        }

        return array(
            "host" => $BDD_HOST,
            "name" => $BDD_NAME,
            "user" => $BDD_USER,
            "pass" => $BDD_PASS
        );
    }

    /**
     * Connexion à la base de données avec PDO
     */
    protected function pdoConnection(): void
    {
        $ENVS = $this->getEnv();

        /**
         * Paramètres PDO
         */
        $BDD_OPTIONS = [
            PDO::ATTR_EMULATE_PREPARES   => false,
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
            PDO::MYSQL_ATTR_FOUND_ROWS   => true
        ];

        try {
            $this->_PDO = new PDO("mysql:host=${ENVS['host']};dbname=${ENVS['name']};charset=utf8mb4", $ENVS['user'], $ENVS['pass'], $BDD_OPTIONS);
        } catch (PDOException $exception) {
            $this->_PDO = null;
            die("Erreur !: " . $exception->getMessage() . "<br/>");
        }

        unset($ENVS);
    }

    /**
     * Retourne tous les items de la base de données
     *
     * @return array
     */
    public function getItems(): array
    {
        $sql = "SELECT id, name, createdAt, updatedAt FROM items;";
        $request = $this->_PDO->prepare($sql);
        $request->execute();
        return $request->fetchAll();
    }

    /**
     * Retourne un item par son ID
     *
     * @param integer $id
     * @return array
     */
    public function getItemByID($id): array
    {
        $itemId = intval($id);

        $sql = "SELECT id, name, createdAt, updatedAt FROM items WHERE items.id = :id;";
        $request = $this->_PDO->prepare($sql);
        $request->bindParam(':id', $itemId);
        $request->execute();
        return $request->fetch();
    }

    /**
     * Supprime un item par son ID
     *
     * @param integer $id
     * @return bool
     */
    public function deleteItemByID($id): bool
    {
        $itemId = intval($id);

        $sql = "DELETE FROM items WHERE items.id = :id;";
        $request = $this->_PDO->prepare($sql);
        $request->bindParam(':id', $itemId);
        $request->execute();

        $isValid = $request->rowCount();

        if ($isValid) return true;
        return false;
    }

    /**
     * Met à jour un item par son ID
     *
     * @param integer $id
     * @param string $name
     * @return bool
     */
    public function updateItemByID($id, $name): bool
    {
        $itemId = intval($id);
        $itemName = htmlspecialchars(strip_tags($name));
        $date = new DateTime('now');
        $updateAtDate = $date->format('Y-m-d H:i:s');

        $sql = "UPDATE items SET items.name = :name, items.updatedAt = :updateAt WHERE items.id = :id";
        $request = $this->_PDO->prepare($sql);
        $request->bindParam(':id', $itemId);
        $request->bindParam(':name', $itemName);
        $request->bindParam(':updateAt', $updateAtDate);
        $request->execute();

        $isValid = $request->rowCount();

        if ($isValid) return true;
        return false;
    }

    /**
     * Ajoute un item dans la base de données
     *
     * @param string $name
     * @return bool
     */
    public function addItem($name): bool
    {
        $itemName = htmlspecialchars(strip_tags($name));
        $date = new DateTime('now');
        $createdAtDate = $date->format('Y-m-d H:i:s');

        $sql = "INSERT INTO items (items.name, items.createdAt) VALUES (:name, :createdAt);";
        $request = $this->_PDO->prepare($sql);
        $request->bindParam(':name', $itemName);
        $request->bindParam(':createdAt', $createdAtDate);
        $request->execute();

        $isValid = $request->rowCount();

        if ($isValid) return true;
        return false;
    }
}


$todos = new ToDO();

if (!empty($_GET) && !empty($_POST)) {
    if (isset($_GET['deleteItem'])) {
        if (isset($_POST['itemID'])) {
            $itemID = intval($_POST['itemID']);

            if ($todos->deleteItemByID($itemID)) {
                header('Location: index.php?code=1');
                return;
            }
            header('Location: index.php?code=0');
            return;
        }
    }

    if (isset($_GET['addItem'])) {
        if (isset($_POST['itemName'])) {
            $itemName = htmlspecialchars(strip_tags($_POST['itemName']));

            if ($todos->addItem($itemName)) {
                header('Location: index.php?code=1');
                return;
            }
            header('Location: index.php?code=0');
            return;
        }
    }

    if (isset($_GET['updateItem'])) {
        if (isset($_POST['itemName']) && isset($_POST['itemID'])) {
            $itemID = intval($_POST['itemID']);
            $itemName = htmlspecialchars(strip_tags($_POST['itemName']));

            if ($todos->updateItemByID($itemID, $itemName)) {
                header('Location: index.php?code=1');
                return;
            }
            header('Location: index.php?code=0');
            return;
        }
    }
}
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>My Todo List</title>

    <style>
        body {
            display: grid;
            place-content: center;

            background-color: #e0e0e0;
        }

        h2 {
            margin-bottom: 20px;
        }
        h2::after {
            content: " ";
            height: 1px;
            width: 100%;

            display: block;

            background-color: silver;
        }
    </style>
</head>
<body>
    <h1>Todo List</h1>

    <h2>Add new item</h2>
    <form action="?addItem" method="post" enctype="multipart/form-data">
        <label>
            Name :
            <input type="text" name="itemName" min="2" max="60" placeholder="Item name" required>
        </label>
        <button type="submit">Add</button>
    </form>

    <h2>Items</h2>
    <ul>
        <?php
            $items = $todos->getItems();
            foreach ($items as $item):
                ?>
                    <li>
                        <h3><?= $item->name ?></h3>
                        <form action="?deleteItem" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="itemID" value="<?= $item->id ?>">
                            <button type="submit">Delete</button>
                        </form>
                        <form action="?updateItem" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="itemID" value="<?= $item->id ?>">
                            <label>
                                Update name :
                                <input type="text" name="itemName" min="2" max="60" placeholder="new name" required>
                            </label>
                            <button type="submit">Update</button>
                        </form>
                    </li>
                <?php
            endforeach;
        ?>
    </ul>
</body>
</html>
