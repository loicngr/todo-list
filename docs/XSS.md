## With XSS 
```php
    /**
     * Ajoute un item dans la base de données
     *
     * @param $itemName
     * @return bool
     */
    public function addItem($itemName): bool
    {
        $date = new DateTime('now');
        $createdAtDate = $date->format('Y-m-d H:i:s');

        $sql = "INSERT INTO items (items.name, items.createdAt) VALUES (:name, :createdAt);";
        $request = $this->_PDO->prepare($sql);
        $request->bindParam(':name', $itemName);
        $request->bindParam(':createdAt', $createdAtDate);
        $request->execute();

        $isValid = $request->rowCount();

        if ($isValid) return true;
        return false;
    }
}

$todos = new ToDO();
if (isset($_GET['addItem'])) {
    if (isset($_POST['itemName'])) {
        $itemName = $_POST['itemName'];

        if ($todos->addItem($itemName)) {
            header('Location: index.php?code=1');
            return;
        }
        header('Location: index.php?code=0');
        return;
    }
}
```

## Without XSS 
```php
    /**
     * Ajoute un item dans la base de données
     *
     * @param $itemName
     * @return bool
     */
    public function addItem($name): bool
    {
        $itemName = htmlspecialchars(strip_tags($name));
        $date = new DateTime('now');
        $createdAtDate = $date->format('Y-m-d H:i:s');

        $sql = "INSERT INTO items (items.name, items.createdAt) VALUES (:name, :createdAt);";
        $request = $this->_PDO->prepare($sql);
        $request->bindParam(':name', $itemName);
        $request->bindParam(':createdAt', $createdAtDate);
        $request->execute();

        $isValid = $request->rowCount();

        if ($isValid) return true;
        return false;
    }
}

$todos = new ToDO();
if (isset($_GET['addItem'])) {
    if (isset($_POST['itemName'])) {
        $itemName = htmlspecialchars(strip_tags($_POST['itemName']));
        
        if ($todos->addItem($itemName)) {
            header('Location: index.php?code=1');
            return;
        }
        header('Location: index.php?code=0');
        return;
    }
}
```